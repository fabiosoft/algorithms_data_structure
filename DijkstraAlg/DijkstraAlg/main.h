//
//  main.h
//  DijkstraAlg
//
using namespace std;

void Dijkstras(vector<Node*> nodes, vector<Edge*> edges);
void DijkstrasTest();
void PrintShortestRouteTo(Node* destination);
void inserisciGrafo();
void mostraMenu();
