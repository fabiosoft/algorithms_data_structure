//
//  main.cpp
//  DijkstraAlg
//
//  Created by Fabio Nisci on 05/01/13.
//  Copyright (c) 2013 Fabiosoft. All rights reserved.
//

/*
 
 (b) Si implementi l’ algoritmo di Dijkstra per cammini minimi. Si verifichi la correttezza del programma su un problema reale.
 
 */


#include <iostream>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include "Node.h"
#include "main.h"


int main(int argc, const char * argv[])
{
    int c;
    mostraMenu();
    
    do{
        // cerca il primo carattere c diverso da spazio
        for( c=getchar() ; isspace(c);c=getchar() );
        switch(c){
            case 'd':  // usa grafo demo
                DijkstrasTest();
                break;
            case 'i': // inserisci manualmente grafo
                inserisciGrafo();
                break;
        }
    }while(c!='q' && c!= 'Q'); /* termina quando c vale "q" oppure "Q" */
    
    return 0;
}


//carica grafo personalizzato da file
void inserisciGrafo(){
    vector<Node*> nodes;
    vector<Edge*> edges;
    
    /**
     * FORMATO:
     * a
     * b
     * c
     * d
     * e
     * f
     * g
     * a,c,1
     * a,d,2
     * b,c,2
     * c,d,1
     * b,f,3
     * c,e,3
     * e,f,2
     * d,g,1
     * g,f,1
     *
     */
    
    std::ifstream file("grafo.txt");
    std::string str;
    while (std::getline(file, str))
    {
        // Process str
        if (str.length() == 1) {
            //un nodo
            Node* nodo = new Node(str.substr(0,1));
            nodes.push_back(nodo);
        }
        
        if (str.length() > 1) {
            int distanza = std::stoi(str.substr(4));
            std::string norig, ndest;
            norig = str.substr(0,1);
            ndest = str.substr(2,1);
            
            Node *orig = Node::trovaNododaID(norig, nodes);
            Node *dest = Node::trovaNododaID(ndest, nodes);
            cout << orig->getid() << " - " << dest->getid() << " - " << distanza << "\n";
            Edge* e1 = new Edge(orig, dest, distanza);
            edges.push_back(e1);
        }
    }
    
    if (edges.size() > 0 && nodes.size() > 0) {
        cout << "Grafo caricato\n";
        //imposta il nodo di partenza
        cout << "Nodo di partenza?\n";
        string start;
        cin >> start;
        Node *nstart = Node::trovaNododaID(start, nodes);
        nstart->setdistanceFromStart(0);
        
        //algoritmo di Dijkstras
        Dijkstras(nodes, edges);
        
        //stampa il percorso minimo
        cout << "Node di destinazione?\n";
        string destinazione;
        cin >> destinazione;
        Node *ndestinazione = Node::trovaNododaID(destinazione, nodes);
        PrintShortestRouteTo(ndestinazione);
    }else{
        cerr << "Errore nel caricare il grafo.";
    }
    
}
void mostraMenu(){
    cout <<
    "-------------------------------------------------\n"<<
    "| Progetto Algoritmi e Strutture Dati\n"<<
    "| Algoritmo Dijkstra\n"<<
    "| Fabio Nisci - Matricola 0124000074 - 2012/2013\n"<<
    "-------------------------------------------------\n"<<
    "\n"
    "d    \tUsa un grafo demo.\n"<<
    "i    \tInserisci manualmente grafo.\n"<<
    "q    \tTermina l\'esecuzione del programma.\n";
}

void DijkstrasTest(){
    vector<Node*> nodes;
    vector<Edge*> edges;
    
    cout << "Grafo:\n\n";
    cout << "           B --------------------------+\n" ;
    cout << "           |                           |\n";
    cout << "           2                           3\n";
    cout << "           |                           |\n";
    cout << "A  ---1--- C ----- 3 --- E --- 2 ----- F\n";
    cout << "|          |                           |\n";
    cout << "|          1                           1\n";
    cout << "|          |                           |\n";
    cout << "+--- 2 --- D -----------1------------- G\n";
    cout << "\n\n";
    
    //dichiarazioni nodi e aggiunta al vettore che
    //contiene tutti i nodi
	Node* a = new Node("a");
    nodes.push_back(a);
	Node* b = new Node("b");
    nodes.push_back(b);
	Node* c = new Node("c");
    nodes.push_back(c);
	Node* d = new Node("d");
    nodes.push_back(d);
	Node* e = new Node("e");
    nodes.push_back(e);
	Node* f = new Node("f");
    nodes.push_back(f);
	Node* g = new Node("g");
    nodes.push_back(g);
    
    //dichiarazioni archi
    //e aggiunta al vettore che contiene gli archi
	Edge* e1 = new Edge(a, c, 1);
    edges.push_back(e1);
	Edge* e2 = new Edge(a, d, 2);
    edges.push_back(e2);
	Edge* e3 = new Edge(b, c, 2);
    edges.push_back(e3);
	Edge* e4 = new Edge(c, d, 1);
    edges.push_back(e4);
	Edge* e5 = new Edge(b, f, 3);
    edges.push_back(e5);
	Edge* e6 = new Edge(c, e, 3);
    edges.push_back(e6);
	Edge* e7 = new Edge(e, f, 2);
    edges.push_back(e7);
	Edge* e8 = new Edge(d, g, 1);
    edges.push_back(e8);
	Edge* e9 = new Edge(g, f, 1);
    edges.push_back(e9);
    
    //imposta il nodo di partenza
    a->setdistanceFromStart(0);
    //algoritmo di Dijkstras
	Dijkstras(nodes, edges);
    //stampa il percorso minimo
 	PrintShortestRouteTo(f);
    
    
    delete a;    delete b;    delete c;
    delete d;    delete e;    delete f;
    delete g;
    
    delete e1;    delete e2;    delete e3;
    delete e4;    delete e5;    delete e6;
    delete e7;    delete e8;    delete e9;
}

//stampa il percorso minimo
void PrintShortestRouteTo(Node* destination){
    Node* previous = destination;
    cout << "Distanza dallo start: "
    << destination->getdistanceFromStart() << endl;
    
	while (previous){
        //finchè esiste un precedente stampo l"id
        cout << previous->getid() << " <- ";
        //avanzo all"indietro
		previous = previous->getprevious();
	}
    cout << endl << endl;
}

/*
 L’algoritmo di Dijkstra permette di poter calcolare i cammini minimi all’interno
 di un grafo
 */
void Dijkstras(vector<Node*> nodes, vector<Edge*> edges){
    //finchè l"array nodi contiene elementi
	while (nodes.size() > 0){
        //estrae il nodo più piccolo
		Node* smallest = Node::ExtractSmallest(nodes);
        //genera un vettore di nodi adiacenti al più piccolo
		vector<Node*>* adjacentNodes = smallest->AdjacentRemainingNodes(smallest, edges, nodes);
        
		const size_t size = adjacentNodes->size();
        //scorro tutti i nodi adiacenti
		for (int i=0; i < size; ++i){
			Node* adjacent = adjacentNodes->at(i);
            //calcolo la distanza tra il minore e il prossimo adiacente
            // distanza = dis(adiacente) + dis(piccolo)
			int distance = adjacent->Distance(smallest, adjacent, edges) + smallest->getdistanceFromStart();
			
            //se la distanza è minore dell"adiacente
			if (distance < adjacent->getdistanceFromStart()){
                //la distanza dell"adiacente dal primo è la distanza calcolata
				adjacent->setdistanceFromStart(distance);
                //il precendete dell"adiacente è il più piccolo
				adjacent->setprevious(smallest);
			}
		}
		delete adjacentNodes;
	}
}


