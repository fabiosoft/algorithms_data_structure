//
//  Node.cpp
//  DijkstraAlg
//
//  Created by Fabio Nisci on 06/01/13.
//  Copyright (c) 2013 Fabiosoft. All rights reserved.
//

#include "Node.h"

//costruttore nodo
Node::Node(string identifier){
    setid(identifier);
    setprevious(NULL);
    setdistanceFromStart(INT_MAX);
}

Node::~Node(){
}

//costruttore arco
Edge::Edge(Node* node1, Node* node2, int distance){
    setnode1(node1);
    setnode2(node2);
    setdistance(distance);
}

Edge::~Edge(){
}

void Edge::setnode1(Node *node1){
    this->node1 = node1;
}
void Edge::setnode2(Node *node2){
    this->node2 = node2;
}
void Edge::setdistance(int distance){
    this->distance = distance;
}

Node* Edge::getNode1(){
    return this->node1;
}
Node* Edge::getNode2(){
    return this->node2;
}
int Edge::getdistance(){
    return this->distance;
}

void Node::setid(string id){
    this->id = id;
}
void Node::setprevious(Node *previous){
    this->previous = previous;
}
void Node::setdistanceFromStart(int distanceFromStart){
    this->distanceFromStart = distanceFromStart;
}

string Node::getid(){
    return this->id;
}
Node* Node::getprevious(){
    return this->previous;
}
int Node::getdistanceFromStart(){
    return this->distanceFromStart;
}

//trova nodo partenda dall'id
Node* Node::trovaNododaID(string id, vector<Node*> nodes){
    Node *node_dest = NULL;
    //cerca nodo con id inserito
    for (int k=0; k < nodes.size(); k++) {
        Node *tmp = nodes.at(k);
        string tmpID = tmp->getid();
        if (tmpID.compare(id) == 0 ) {
            node_dest = tmp;
        }
    }
    return node_dest;
}

//trova il nodo con la distanza minima
//lo ritorna e lo rimuove
Node* Node::ExtractSmallest(vector<Node*>& nodes){
	size_t size = nodes.size();
	if (size == 0) return NULL;
	int smallestPosition = 0;
    //imposta il primo come più piccolo
	Node* smallest = nodes.at(0);
	for (int i=1; i<size; ++i){
		Node* current = nodes.at(i);
		if (current->distanceFromStart < smallest->distanceFromStart){
            //se la distanza del nodo corrente è minore della distanza del più piccolo
            //il piccolo diventa il corrente e salvo la posizione
			smallest = current;
			smallestPosition = i;
		}
	}
    //rimuove dal vettore il più piccolo
	nodes.erase(nodes.begin() + smallestPosition);
    //ritorna il nodo più piccolo
	return smallest;
}

//ritorna tutti i nodi adiacenti al 'nodo' corrente
vector<Node*>* Node::AdjacentRemainingNodes(Node* node,vector<Edge*> edges,vector<Node*> nodes){
    //vettore di nodi adiacenti da ritornare
	vector<Node*>* adjacentNodes = new vector<Node*>();
    //numero di archi del grafo
	const size_t size = edges.size();
	for(int i=0; i<size; ++i){
        //scorro tutti gli archi alla ricerca degli adiacenti di 'nodo'
		Edge* edge = edges.at(i);
		Node* adjacent = NULL;
        
        //se il nodo1 dell'arco è il 'nodo'
		if (edge->getNode1() == node){
            //allora l'adiacente di 'nodo' è
            //sicuramente il nodo2
			adjacent = edge->getNode2();
		}
        //se il nodo2 dell'arco è il 'nodo'
		else if (edge->getNode2() == node){
            //allora l'adiacente di 'nodo' è
            //sicuramente il nodo1
			adjacent = edge->getNode1();
		}
        //se l'adiacente esiste e il vettore dei nodi lo contiene
		if (adjacent && Contains(nodes, adjacent)){
            //allora lo aggiungo al vettore degli adiacenti da ritornare
			adjacentNodes->push_back(adjacent);
		}
	}
	return adjacentNodes;
}

//controlla se il vettore dei nodi contiene il 'nodo'
bool Node::Contains(vector<Node*>& nodes, Node* node){
	const size_t size = nodes.size();
    //scorro il vettore dei nodi alla ricerca di 'nodo'
	for(int i=0; i<size; ++i){
		if (node == nodes.at(i)){
			return true;
		}
	}
	return false;
}

//ritorna true se i due nodi sono connessi
bool Edge::Connects(Node* node1, Node* node2){
    return (
            (node1 == this->node1 && node2 == this->node2) ||
            (node1 == this->node2 && node2 == this->node1));
}

//ritorna la distanza tra due nodi connessi
int Node::Distance(Node* node1, Node* node2,vector<Edge*> edges){
	const size_t size = edges.size();
    //scorro gli archi
	for(int i=0; i<size; ++i){
		Edge* edge = edges.at(i);
        //se i due nodi sono connessi
		if (edge->Connects(node1, node2)){
            //ritorno la distanza
			return edge->getdistance();
		}
	}
    //nodi non connessi - errore
	return -1;
}