//
//  Node.h
//  DijkstraAlg
//
//  Created by Fabio Nisci on 06/01/13.
//  Copyright (c) 2013 Fabiosoft. All rights reserved.
//

#include <vector>
#include <string>
using namespace std;

class Node;
class Edge;

class Node{
private:
    string id;
	Node* previous;
	int distanceFromStart;
    
public:
	Node(string identifier);
    ~Node();
    
    //setters
    void setid(string id);
    void setprevious(Node *previous);
    void setdistanceFromStart(int distanceFromStart);
    
    //getters
    string getid();
    Node* getprevious();
    int getdistanceFromStart();
    
    //methods
    vector<Node*>* AdjacentRemainingNodes(Node* node,vector<Edge*> edges,vector<Node*> nodes);
    static Node* ExtractSmallest(vector<Node*>& nodes);
    int Distance(Node* node1, Node* node2,vector<Edge*> edges);
    bool Contains(vector<Node*>& nodes, Node* node);
    static Node* trovaNododaID(string id, vector<Node*> nodes);
};


class Edge{
private:
	Node* node1;
	Node* node2;
	int distance;
    
public:
	Edge(Node* node1, Node* node2, int distance);
    ~Edge();
    //setters
    void setnode1(Node *node1);
    void setnode2(Node *node2);
    void setdistance(int distance);
    
    //getters
    Node* getNode1();
    Node* getNode2();
    int getdistance();
    
    //methods
    bool Connects(Node* node1, Node* node2);
};
