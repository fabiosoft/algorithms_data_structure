###### Fabio Nisci
# Progetto di programmazione
Progetti di programmazione per corso: Algoritmi e strutture dati dell'Università Parthenope di Napoli

* Algoritmo di Dijkstra
* Vocabolario Hash Table

# Programming assignment
Programming assignments for: Algorithm and Data Structure course of Parthenope University of Naples

* Dijkstra's algorithm
* Hash Table dictionary